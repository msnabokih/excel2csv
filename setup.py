from setuptools import find_packages, setup

setup(
    name='excel2csv',
    version='0.1.0a7',
    description='Конвертирование файлов .xls и .xlsx в поток.',
    author='Maksim Nabokikh',
    author_email='nms@bfg-soft.com',
    url='https://git.bfg-soft.ru/projects/TOOL/repos/excel2csv',
    packages=find_packages(exclude=[
        "excel2csv.tests*",
    ]),
    python_requires='>=3.5',
    include_package_data=True,
    install_requires=[
        'openpyxl>=2.5.0,<2.6.0',
        'xlrd>=1.1.0,<1.2.0',
    ],
    extras_require={
        'dev': [
            'mock>=2.0.0',
            'pytest>=2.3.2'
        ]
    },
    test_suite='tests',
)
