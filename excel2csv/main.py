from os.path import isfile

from .converters import convert_xls, convert_xlsx
from .exc import InputExtensionError, InputFileDoesNotExistError
from .utils import get_file_extension

__all__ = [
    'convert',
    'convert_from_file'
]

_TYPE_CONVERTERS = {
    '.xls': convert_xls,
    '.xlsx': convert_xlsx
}


def convert(input_stream, ext, output_stream=None, sheet_identity=0):
    ext = ext.lower() if isinstance(ext, str) else None

    if ext not in _TYPE_CONVERTERS:
        raise InputExtensionError(
            'Указано недопустимое расширение для входных данных.'
        )

    converter = _TYPE_CONVERTERS[ext]
    return converter(input_stream, output_stream, sheet_identity)


def convert_from_file(file_path, output_stream=None, sheet_identity=0):
    if not isfile(file_path):
        raise InputFileDoesNotExistError(
            "Указанный входной файл не существует."
        )
    ext = get_file_extension(file_path)

    with open(file_path, 'rb') as f:
        return convert(f, ext, output_stream, sheet_identity)
