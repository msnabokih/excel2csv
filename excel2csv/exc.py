
__all__ = [
    'Excel2CSVError',
    'InputExtensionError',
    'InputIsNotStreamError',
    'OutputIsNotStreamError',
    'InputSheetIsNotValidTypeError',
    'InputFileDoesNotExistError'
]


class Excel2CSVError(Exception):
    """Корневое исключение."""


class InputExtensionError(Excel2CSVError):
    """Входной файл имеет неподходящее разрешение."""


class InputIsNotStreamError(Excel2CSVError):
    """Входные данные не являются потоком."""


class OutputIsNotStreamError(Excel2CSVError):
    """Выходные данные не являются потоком."""


class InputSheetIsNotValidTypeError(Excel2CSVError):
    """В качестве индентификатора листа нужно указать строку или число."""


class InputFileDoesNotExistError(Excel2CSVError):
    """Указанный входной файл не существует."""
