from datetime import date, time, datetime, timedelta

from xlrd import xldate_as_datetime

from ..constant import NULL_DATETIME_VALUE, \
    MICROSECONDS_ROUNDING_BORDERS, NULL_TIME, EXCEL_DATE_SYSTEM_START_POINT
from ..utils import has_date_elements

__all__ = [
    'normalize_date_xls',
    'normalize_datetime_xlsx'
]


def normalize_date_xls(value):
    if not value:
        return

    v_tuple = xldate_as_datetime(value, 0).timetuple()

    if v_tuple[3:6] == NULL_DATETIME_VALUE:
        # Только дата
        return date(*v_tuple[:3])

    elif v_tuple[:3] == NULL_DATETIME_VALUE:
        return time(*v_tuple[3:6])

    # Дата и время
    return datetime(*v_tuple[:6])


def normalize_datetime_xlsx(cell):
    """
    Функция для округления микросекунд до секунд.
    Если их нет - округлять нечего. Если значение меньше нижней границы -
    округляем до нуля. Если значение больше верхней границы - округляем до
    одной секунды..
    """
    dt = cell.value

    if dt.time() == NULL_TIME:
        return dt.date()

    if dt.date() == EXCEL_DATE_SYSTEM_START_POINT \
            and not has_date_elements(cell):
        dt = dt.time()

    ms = dt.microsecond

    if ms == 0:
        return dt

    if ms < MICROSECONDS_ROUNDING_BORDERS.MIN:
        return dt.replace(microsecond=0)
    elif ms > MICROSECONDS_ROUNDING_BORDERS.MAX:
        return dt.replace(microsecond=0) + timedelta(seconds=1)
    return dt
