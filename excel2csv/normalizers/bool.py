
__all__ = [
    'normalize_boolean'
]


def normalize_boolean(value):
    if value is None or value == '':
        return
    return bool(value)
