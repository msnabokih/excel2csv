
__all__ = [
    'normalize_number'
]


def normalize_number(value):
    if value and value % 1 != 0:
        return value
    return int(value)

