from collections import namedtuple
from datetime import time, date

__all__ = [
    'NULL_TIME',
    'EXCEL_DATE_SYSTEM_START_POINT',
    'NULL_DATETIME_VALUE',
    'MICROSECONDS_ROUNDING_BORDERS'
]

NULL_TIME = time(0, 0, 0)

EXCEL_DATE_SYSTEM_START_POINT = date(1904, 1, 1)

NULL_DATETIME_VALUE = (0, 0, 0)

MICROSECONDS_ROUNDING_BORDERS = namedtuple(
    'MICROSECONDS_ROUNDING_BORDERS', ('MIN', 'MAX')
)(1000, 999000)
