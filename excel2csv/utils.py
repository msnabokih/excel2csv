from collections import Sequence
from os.path import splitext

from xlrd import XL_CELL_EMPTY, XL_CELL_TEXT

__all__ = [
    'is_sequence',
    'has_date_elements',
    'get_file_extension'
]


def is_sequence(obj):
    return isinstance(obj, Sequence) and not isinstance(obj, str)


def has_date_elements(cell):
    """
    Пытаемся применить форматирование, если ячейка содержит только time info.
    """
    return 'd' in cell.number_format or 'y' in cell.number_format


def get_file_extension(path):
    return splitext(path)[1].lower()
