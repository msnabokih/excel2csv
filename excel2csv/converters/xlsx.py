from datetime import datetime
from functools import partial

import openpyxl

from .base import converter_factory
from ..normalizers import normalize_datetime_xlsx

__all__ = [
    'convert_xlsx'
]


def _get_cell_value(cell):
    value = cell.value

    if not isinstance(value, datetime):
        return value

    return normalize_datetime_xlsx(cell)


def _xlsx_book_reader(file):
    return openpyxl.load_workbook(file, read_only=True, data_only=True)


def _xlsx_csv_writer(book, sheet, writer):
    if isinstance(sheet, str):
        sheet = book[sheet]
    else:
        sheet = book.worksheets[sheet]

    writer.writerows(
        (_get_cell_value(item) for item in row) for row in sheet
    )


convert_xlsx = partial(
    converter_factory,
    _xlsx_book_reader,
    _xlsx_csv_writer
)
