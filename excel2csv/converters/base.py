import csv
from io import StringIO, BufferedIOBase, TextIOBase

from ..exc import InputIsNotStreamError, OutputIsNotStreamError, \
    InputSheetIsNotValidTypeError

__all__ = [
    'converter_factory'
]


def converter_factory(
        book_loader, csv_writer,
        input_stream, output_stream=None, sheet_identity=0
):
    """
    :param book_loader: функция для считывания книги эксель
    :param csv_writer: функция для записи листов эксель в csv
    :param input_stream: входной поток файла excel
    :param output_stream: выходной поток, в который мы запишем получившиеся
    данные
    :param sheet_identity: лист, который необходимо обработать
    (имя листа или его порядковый индекс)
    """

    if output_stream is None:
        output_stream = StringIO()

    if not isinstance(input_stream, BufferedIOBase):
        raise InputIsNotStreamError(
            'Входные данные не являются потоком, подходящим для чтения.'
        )

    if not isinstance(output_stream, TextIOBase):
        raise OutputIsNotStreamError(
            'Выходные данные не являются потоком, пригодным для записи.'
        )

    if not isinstance(sheet_identity, (str, int)):
        raise InputSheetIsNotValidTypeError(
            'В качестве идентификатора листа нужно указать строку или число.'
        )

    # Для удобного форматирования в csv инициализируем здесь csv writer
    writer = csv.writer(output_stream)

    csv_writer(book_loader(input_stream), sheet_identity, writer)
    return output_stream
