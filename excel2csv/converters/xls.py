from functools import partial

from xlrd import open_workbook, XL_CELL_BOOLEAN, XL_CELL_DATE, XL_CELL_NUMBER

from .base import converter_factory
from ..normalizers import normalize_boolean, normalize_date_xls, \
    normalize_number


__all__ = [
    'convert_xls'
]


def _get_normalized_row(sheet, row_index):
    for col_index in range(sheet.ncols):
        value = sheet.cell_value(row_index, col_index)
        excel_type = sheet.cell_type(row_index, col_index)

        if excel_type == XL_CELL_BOOLEAN:
            yield normalize_boolean(value)
        elif excel_type == XL_CELL_DATE:
            yield normalize_date_xls(value)
        elif excel_type == XL_CELL_NUMBER:
            yield normalize_number(value)
        else:
            yield value


def _xls_book_reader(file):
    # Смещаем указатель положения, т.к. в отличие от openpyxl библиотека
    # xlrd сама файл не перечитывает
    file.seek(0)
    return open_workbook(file_contents=file.read())


def _xls_csv_writer(book, sheet, writer):
    if isinstance(sheet, str):
        sheet = book.sheet_by_name(sheet)
    else:
        sheet = book.sheet_by_index(sheet)

    writer.writerows(
        map(partial(_get_normalized_row, sheet), range(sheet.nrows))
    )


convert_xls = partial(
    converter_factory,
    _xls_book_reader,
    _xls_csv_writer
)
