import csv
import os

import pkg_resources
import pytest

from excel2csv import convert_from_file


@pytest.fixture()
def get_mock():
    mock_root_dirpath = pkg_resources.resource_filename(
        'excel2csv', 'tests/mock_'
    )

    def get_path(*subpath):
        return os.path.join(mock_root_dirpath, *subpath)

    return get_path


@pytest.fixture()
def test_excel():
    def test_(input_filepath, answer_filepath, sheet_identity):
        output_stream = convert_from_file(
            input_filepath, sheet_identity=sheet_identity
        )

        result = output_stream.getvalue() \
            .replace('"', '').replace('\n', '').split('\r')

        with open(answer_filepath) as f:
            answer = csv.reader(f)
            answer = [','.join(row) for row in answer]
            answer.append('')

        assert result == answer

    return test_
