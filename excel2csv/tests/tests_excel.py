def test_xls(get_mock, test_excel):
    input_filepath = get_mock('test_xls.xls')
    answer_filepath = get_mock('test_xls_answer.csv')

    test_excel(input_filepath, answer_filepath, 'Sheet3')


def test_xlsx(get_mock, test_excel):
    input_filepath = get_mock('test_xlsx.xlsx')
    answer_filepath = get_mock('test_xlsx_answer.csv')

    test_excel(input_filepath, answer_filepath, 'Sheet3')
